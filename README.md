# EpiToPass

Micro service qui va chercher des fichiers sur les serveurs d'Epiconcept par webservice et qui les dépose sur un répertoire défini par la configuration.

### API Epifiles

- Doc api epifiles: https://epiconcept-paris.github.io/epidocs/epifiles_ws.html#webservices-depifiles
- Lien vers l'API: https://epifiles.voozanoo.net/epifiles/ws
- Portail web: https://epifiles.voozanoo.net/epifiles/auth/index/localconn
- Login: ssp_dev_api

Les identifiants pour se connecter à l'API sont les mêmes que ceux pour se connecter sur l'interface web.
Pour se connecter à l'API, il faut encoder en base64 "username:password" et passer ça en authentification basic.

### Builder le projet

Avec maven, depuis le répertoire racine du projet:
```shell
mvn package
```

Un jar exécutable est créé dans le répertoire `./target`.

### Lancer le jar et configurer l'application

Pas de profil spring.

Liste des variables de configuration :
```properties
# Fréquence du cron            // toutes les minutes
app.cron.transfertFiles = 0 * * * * *

# Url, username et password pour accéder à l'api epifiles
app.source.apiBaseUrl  = https://xxxx.voozanoo.net/epifiles/ws
app.source.apiUsername = tototiti
app.source.apiPassword = **********

#
# Répertoires où seront copiés les fichiers téléchargés
# Le répertoire doit exister au lancement de l'application
# sinon une erreur sera levée
app.target.outputDirectoryPath = /home/nath/Documents/tmp/epi2pass

#
# Configuration du registre des fichiers téléchargés
# Pour éviter de télécharger 2 fois le même fichier, 
# à la fin du téléchargement, l'id du fichier est enregistré dans un registre
# il s'agit d'un fichier text qu'on configure ci-dessous
# Si le fichier n'existe pas au lancement de l'application, il sera créé
# par contre si le répertoire n'existe pas, une erreur sera levée
app.target.registerDirectory = /home/nath/Documents/tmp/epi2pass
app.target.registerFileName  = epiToPass-log.txt
```

Pour modifier une variable, il faut la setter au lancement du jar (ou la modifier dans le fichier `application.yml` et rebuilder).

Exemple de lancement du jar :
```shell
// Toutes les heures
java \
    -Dapp.cron.transfertFiles="0 0 * * * *" \
    -Dapp.source.apiBaseUrl="https://epifiles.voozanoo.net/epifiles/ws" \
    -Dapp.source.apiUsername="ssp_dev_api" \
    -Dapp.source.apiPassword="monmotdepasse" \
    -Dapp.target.outputDirectoryPath="/home/nath/Documents/tmp/epi2pass" \
    -Dapp.target.registerDirectory="/home/nath/Documents/tmp/epi2pass/subregistre" \
    -Dapp.target.registerFileName="registre.txt" \
    -jar EpiToPass-0.0.1-SNAPSHOT.jar
```



